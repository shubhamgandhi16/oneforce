package salesforce.smoke.tests;

import static utilities.MyExtentReports.reports;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentTest;

import atu.testng.reports.ATUReports;
import atu.testng.reports.logging.LogAs;
import atu.testng.selenium.reports.CaptureScreen;
import atu.testng.selenium.reports.CaptureScreen.ScreenshotOf;
import driverfactory.Driver;
import pages.SalesForce.SF_ContactsPage;
import pages.SalesForce.SF_Header;
import pages.SalesForce.SF_LoginPage;
import pages.SalesForce.SF_SearchPage;
import utilities.InitTests;
import verify.SoftAssertions;

public class TestOpportunities extends InitTests{
	Driver driverFact= new Driver();
	WebDriver driver = null;
	WebDriver webdriver = null;
	
	@BeforeClass
	public void beforeClass() throws Exception{
		webdriver=driverFact.initWebDriver("","CHROME","local","");
	}
	
	@Test(enabled = true, priority = 1)
	public void createOpportunity() throws Exception {
		ExtentTest test=null;
		try {
			 test = reports.createTest("testAccounts");
			test.assignCategory("smoke");
			driver=driverFact.getEventDriver(webdriver,test);
			SF_LoginPage sfLogin=new SF_LoginPage(driver);
			SF_Header sfHeader=new SF_Header(driver);
			SF_ContactsPage sfContacts =new SF_ContactsPage(driver);
			
			sfLogin.login(USERNAME, PASSWORD);
			sfHeader.checkHeaderElements();
			sfHeader.selectContactsFromDropdown(driver);
			sfContacts.testContactElements(driver);
			sfContacts.selectExistingFirstContact(driver);
			sfContacts.selectNewOpportunity(driver);
			sfContacts.createNewOpportunityAndSave(driver);
			sfContacts.checkNewOpportunityTitleAfterCreating(driver, test);
					
		} catch (Error e) {
			e.printStackTrace();
			SoftAssertions.fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()), test);
			ATUReports.add("testSearch()", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
			softAssert.assertAll();
			
		} catch (Exception e) {
			e.printStackTrace();
			SoftAssertions.fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()), test);
			ATUReports.add("testSearch()", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
			softAssert.assertAll();
		} 
		finally{
			reports.flush();
		}
	}
	
	@Test(enabled = true, priority= 2)
	public void viewCreatedOpportunity() throws Exception {
		ExtentTest test=null;
		try {
			 test = reports.createTest("testAccounts");
			test.assignCategory("smoke");
			driver=driverFact.getEventDriver(webdriver,test);
			SF_Header sfHeader=new SF_Header(driver);
			SF_ContactsPage sfContacts =new SF_ContactsPage(driver);
			SF_SearchPage sfSearch = new SF_SearchPage(driver);
			
			sfContacts.searchCreatedOpportunity();
			sfHeader.closeFirstTab(driver);
			sfSearch.testSearchSalesforceResultOpportunity(driver);
			
		} catch (Error e) {
			e.printStackTrace();
			SoftAssertions.fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()), test);
			ATUReports.add("testSearch()", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
			softAssert.assertAll();
			
		} catch (Exception e) {
			e.printStackTrace();
			SoftAssertions.fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()), test);
			ATUReports.add("testSearch()", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
			softAssert.assertAll();
		} 
		finally{
			reports.flush();
			driver.close();
		}
	}
}
