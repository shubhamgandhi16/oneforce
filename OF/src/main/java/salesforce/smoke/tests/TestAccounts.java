package salesforce.smoke.tests;

import utilities.InitTests;
import verify.SoftAssertions;
import static utilities.MyExtentReports.reports;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentTest;

import atu.testng.reports.ATUReports;
import atu.testng.reports.logging.LogAs;
import atu.testng.selenium.reports.CaptureScreen;
import atu.testng.selenium.reports.CaptureScreen.ScreenshotOf;
import driverfactory.Driver;
import pages.SalesForce.SF_Accounts;
import pages.SalesForce.SF_Dashboard;
import pages.SalesForce.SF_Header;
import pages.SalesForce.SF_LoginPage;
import pages.SalesForce.SF_SearchPage;

public class TestAccounts extends InitTests{
	SF_LoginPage loginPage;
	Driver driverFact= new Driver();
	WebDriver driver = null;
	WebDriver webdriver = null;
	
	@BeforeClass
	public void beforeClass() throws Exception{
		webdriver=driverFact.initWebDriver("","CHROME","local","");
	}
	
	@Test(enabled = true, priority = 1)
	public void testAccount() throws Exception {
		ExtentTest test=null;
		try {
			 test = reports.createTest("testAccounts");
			test.assignCategory("smoke");
			driver=driverFact.getEventDriver(webdriver,test);
			SF_LoginPage sfLogin=new SF_LoginPage(driver);
			SF_Dashboard sfDashboard = new SF_Dashboard(driver);
			SF_Header sfHeader=new SF_Header(driver);
			SF_Accounts sfAccounts =new SF_Accounts(driver);
			SF_SearchPage sfSearchPage = new SF_SearchPage(driver);
			
			sfLogin.login(USERNAME, PASSWORD);
			sfDashboard.checkDashboardElements(driver, test);
			sfHeader.checkHeaderElements();
			sfHeader.selectAccountsFromDropdown(driver);
			sfAccounts.checkAccountElements(driver);
			sfHeader.searchingAnAccount();
			sfSearchPage.checkSearchSalesforceAccountResult(driver);
			sfSearchPage.clickSearchedAccount(driver);
			sfHeader.closeFirstTab(driver);
			sfAccounts.testSelectedAccountTitle(driver, test);
			
		} catch (Error e) {
			e.printStackTrace();
			SoftAssertions.fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()), test);
			ATUReports.add("testSearch()", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
			softAssert.assertAll();
			
		} catch (Exception e) {
			e.printStackTrace();
			SoftAssertions.fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()), test);
			ATUReports.add("testSearch()", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
			softAssert.assertAll();
		} 
		finally{
			reports.flush();
			driver.close();
		}
	}
}
