package pages.SalesForce;

import static driverfactory.Driver.*;
import static pages.SalesForce.SF_ContactsPage.*;
import static pages.SalesForce.SF_Header.*;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class SF_SearchPage {
	//public static String accountName= "AutomationTest Account";
	//public static String accountName= "21st Century Fox";
	
	@FindBy(xpath="(//iframe[contains(@id, 'ext-comp')])[2]")
	public static WebElement accountSearchiFrame;
	@FindBy(xpath="//li//a[contains(@title, 'Accounts')]")
	public static WebElement searchAccountSelectRecordAccounts;
	
	@FindBy(xpath="//td[@class='actionColumn']/following::a[1]")
	public static WebElement firstSearchedContact;
	@FindBy(xpath="(//iframe[contains(@id, 'ext-comp')])[2]")
	public static WebElement contactSearchiFrame;
	@FindBy(xpath="//li//a[contains(@title, 'Contacts')]")
	public static WebElement searchContactsSelectRecordContacts;
	@FindBy(xpath="//li//a[contains(@title, 'Opportunities')]")
	public static WebElement searchContactsSelectRecordOpportunities;
	
	public SF_SearchPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}
	
	public void checkSearchSalesforceAccountResult(WebDriver driver) throws InterruptedException{
		switchToFrame(driver, accountSearchiFrame);
		clickElement(searchAccountSelectRecordAccounts);
		driver.findElement(By.xpath("//a[contains(text(), '"+accountName+"')]")).isDisplayed();
		switchToDefaultContent(driver);
	}
	
	public void clickSearchedAccount(WebDriver driver) throws InterruptedException{
		switchToFrame(driver, accountSearchiFrame);
		driver.findElement(By.xpath("//a[contains(text(), '"+accountName+"')]")).click();
		switchToDefaultContent(driver);
	}
	
	public void checkSearchSalesforceResultContact(WebDriver driver) throws InterruptedException{
		switchToFrame(driver, contactSearchiFrame);
		clickElement(searchContactsSelectRecordContacts);
		driver.findElement(By.xpath("//a[contains(text(), '"+expectedPageDescription+"')]")).isDisplayed();
		switchToDefaultContent(driver);
	}
	
	public void clickSearchedContact(WebDriver driver) throws InterruptedException{
		switchToFrame(driver, contactSearchiFrame);
		driver.findElement(By.xpath("//a[contains(text(), '"+expectedPageDescription+"')]")).click();
		switchToDefaultContent(driver);
	}
	
	public void testEditedContact(WebDriver driver) throws InterruptedException{
		Thread.sleep(3000);
		switchToFrame(driver, contactSearchiFrame);
		clickElement(searchContactsSelectRecordContacts);
		driver.findElement(By.xpath("//a[contains(text(), '"+editedContactName+"')]")).isDisplayed();
		switchToDefaultContent(driver);
	}
	
	public void checkExistingSalesforceResultContact(WebDriver driver) throws InterruptedException{
		switchToFrame(driver, contactSearchiFrame);
		clickElement(searchContactsSelectRecordContacts);
		driver.findElement(By.xpath("//a[contains(text(), '"+searchExistingContact+"')]")).isDisplayed();
		switchToDefaultContent(driver);
	}
	
	public void clickExistingContact(WebDriver driver) throws InterruptedException{
		switchToFrame(driver, contactSearchiFrame);
		driver.findElement(By.xpath("//a[contains(text(), '"+searchExistingContact+"')]")).click();
		switchToDefaultContent(driver);
	}
	
	public void testSearchSalesforceResultOpportunity(WebDriver driver) throws InterruptedException{
		switchToFrame(driver, contactSearchiFrame);
		clickElement(searchContactsSelectRecordOpportunities);
		driver.findElement(By.xpath("//a[contains(text(), '"+opportunityName+"')]")).isDisplayed();
		switchToDefaultContent(driver);
	}

}
