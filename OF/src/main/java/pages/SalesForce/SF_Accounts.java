package pages.SalesForce;

import static driverfactory.Driver.*;
import static pages.SalesForce.SF_ContactsPage.*;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.aventstack.extentreports.ExtentTest;

import static verify.SoftAssertions.*;

public class SF_Accounts {
	
	@FindBy(id="phSearchInput")
	public static WebElement searchSalesForce;
	
	@FindBy(id="ext-comp-1005")
	public static WebElement iframe1Switch;
	
	@FindBy(xpath="//select[contains(@id, 'listSelect')]")
	public static WebElement accountsSelect;
	
	@FindBy(xpath="//select[contains(@id, 'listSelect')]/option[contains(text(),'All Accounts')]")
	public static WebElement selectAllAccounts;
	@FindBy(xpath="//select[contains(@id, 'listSelect')]/option[contains(text(),'Carrier Accounts')]")
	public static WebElement selectCareerAccounts;
	@FindBy(xpath="//select[contains(@id, 'listSelect')]/option[contains(text(),'Client Accounts')]")
	public static WebElement selectClientAccounts;
	@FindBy(xpath="//select[contains(@id, 'listSelect')]/option[contains(text(),'My Accounts')]")
	public static WebElement selectMyAccounts;
	@FindBy(xpath="//select[contains(@id, 'listSelect')]/option[contains(text(),'Recently Viewed Accounts')]")
	public static WebElement selectRecentlyViewedAccounts;
	
	@FindBy(xpath="//*[@title='Account Name']")
	public static WebElement titleAccountName;
	@FindBy(xpath="//*[@id='ext-gen10']//table//td[3]//a")
	public static WebElement recordAccountName;
	
	
	@FindBy(xpath="(//iframe[contains(@id, 'ext-comp')])[2]")
	public static WebElement accountPageiFrame;
	@FindBy(xpath="//*[@class='pageDescription']")
	public static WebElement accountPageTitle;
	
	public SF_Accounts(WebDriver driver){
		PageFactory.initElements(driver, this);
	}
	
	public void checkAccountElements(WebDriver driver){
		switchToFrame(driver, iframe1Switch);
		waitForElementToDisplay(accountsSelect);
		waitForElementToDisplay(titleAccountName);
		waitForElementToDisplay(recordAccountName);
		switchToDefaultContent(driver);
	}
	
	public void testSelectedAccountTitle(WebDriver driver, ExtentTest test) throws InterruptedException{
		switchToFrame(driver, accountPageiFrame);
		verifyElementText(accountPageTitle, accountName, test);
		switchToDefaultContent(driver);
	}
}
