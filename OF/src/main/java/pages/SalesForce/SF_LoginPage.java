package pages.SalesForce;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import static driverfactory.Driver.waitForElementToDisplay;
import static driverfactory.Driver.setInput;
import static driverfactory.Driver.clickElement;

public class SF_LoginPage {

	@FindBy(css= "input[id='username']")
	public static WebElement usernameInput;
	@FindBy(css= "input[id='password']")
	public static WebElement passwordInput;
	@FindBy(css= "input[id='Login']")
	public static WebElement loginBtn;
	
	public SF_LoginPage(WebDriver driver){
		PageFactory.initElements(driver, this);
	}
	
	public void login(String username, String password) throws InterruptedException{
		waitForElementToDisplay(loginBtn);
		setInput(usernameInput, username);
		setInput(passwordInput, password);
		clickElement(loginBtn);
	}
}
