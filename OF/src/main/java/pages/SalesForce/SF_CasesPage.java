package pages.SalesForce;

import static driverfactory.Driver.*;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import static pages.SalesForce.SF_ContactsPage.*;
import com.aventstack.extentreports.ExtentTest;

import static verify.SoftAssertions.*;

public class SF_CasesPage {
	static String timestamp= getCurrTimeStamp();
	String caseType= "VB - BC";
	String caseOwnerName= "Sindhu Ganesan";
	static String callerInfoComments= "autoComment"+timestamp;
	
	@FindBy(xpath="(//iframe[contains(@id, 'ext-comp')])[1]")
	public static WebElement casesHomeiframe;
	@FindBy(xpath="//select[contains(@class, 'title')]")
	public static WebElement casesSelect;
	@FindBy(xpath="(//span[contains(text(), 'AutomationTest Account')]/preceding::td[contains(@class, 'CASE_NUMBER')])[2]//a")
	public static WebElement firstCase;
	
	@FindBy(xpath="(//iframe[contains(@id, 'ext-comp')])[2]")
	public static WebElement viewCaseiframe;
	@FindBy(xpath="//*[@class='bPageTitle']//img[@title='Case']")
	public static WebElement viewCase_PageTitle;
	@FindBy(xpath="//a[text()='[Change]']")
	public static WebElement viewCase_caseOwnerChangeBtn;
	@FindBy(xpath="//input[@title='Edit']")
	public static WebElement viewCase_editCaseBtn;
	@FindBy(xpath="//td[contains(text(), 'Comments')]/following::td/*")
	public static WebElement viewCase_callerInfoComments;
	
	@FindBy(xpath="(//iframe[contains(@id, 'ext-comp')])[2]")
	public static WebElement changeCaseOwneriframe;
	@FindBy(xpath="//input[@id='newOwn']")
	public static WebElement viewCase_changeCaseOwnerInput;
	@FindBy(xpath="//input[@title='Save']")
	public static WebElement viewCase_changeCaseOwner_SaveBtn;
	
	@FindBy(xpath="(//iframe[contains(@id, 'ext-comp')])[2]")
	public static WebElement editCaseiframe;
	@FindBy(xpath="//td/textarea")
	public static WebElement editCase_callerInfoComments;
	@FindBy(xpath="//input[@title='Save']")
	public static WebElement editCase_saveBtn;
	
	public SF_CasesPage(WebDriver driver){
		PageFactory.initElements(driver, this);
	}
	
	public void selectCasesType(WebDriver driver) throws InterruptedException{
		switchToFrame(driver, casesHomeiframe);
		waitForElementToDisplay(casesSelect);
		selEleByVisbleText(casesSelect, caseType);
		switchToDefaultContent(driver);
	}
	
	public void clickTheFirstCreatedCase(WebDriver driver) throws InterruptedException{
		switchToFrame(driver, casesHomeiframe);
		driver.findElement(By.xpath("(//span[contains(text(), '"+accountName+"')]/preceding::td[contains(@class, 'CASE_NUMBER')])[2]//a")).click();
		//clickElement(firstCase);
		switchToDefaultContent(driver);
	}
	
	public void verifyOpenedCasePage(WebDriver driver){
		switchToFrame(driver, viewCaseiframe);
		waitForElementToDisplay(viewCase_PageTitle);
		switchToDefaultContent(driver);
	}
	
	public void clickOnChangeBtn(WebDriver driver) throws InterruptedException{
		switchToFrame(driver, viewCaseiframe);
		clickElement(viewCase_caseOwnerChangeBtn);
		switchToDefaultContent(driver);
	}
	
	public void changeTheCaseOwner(WebDriver driver) throws InterruptedException{
		switchToFrame(driver, changeCaseOwneriframe);
		setInput(viewCase_changeCaseOwnerInput, caseOwnerName);
		clickElement(viewCase_changeCaseOwner_SaveBtn);
		switchToDefaultContent(driver);
	}
	
	public void clickOnEditCaseBtn(WebDriver driver) throws InterruptedException{
		switchToFrame(driver, viewCaseiframe);
		clickElement(viewCase_editCaseBtn);
		switchToDefaultContent(driver);
	}
	
	public void editTheCaseAndSave(WebDriver driver) throws InterruptedException{
		switchToFrame(driver, editCaseiframe);
		setInput(editCase_callerInfoComments, callerInfoComments);
		clickElement(editCase_saveBtn);
		switchToDefaultContent(driver);
	}
	
	public void testTheEditCase(WebDriver driver, ExtentTest test) {
		switchToFrame(driver, viewCaseiframe);
		verifyElementText(viewCase_callerInfoComments, callerInfoComments, test);
		switchToDefaultContent(driver);
	}
}
