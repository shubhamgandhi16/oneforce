package pages.SalesForce;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;


import static driverfactory.Driver.clickElement;
import static driverfactory.Driver.waitForElementToDisplay;


public class HeaderPage {
  
 
    @FindBy(id = "headerlogout")
    public static WebElement logout;
    @FindBy(id = "TE_HEADER_AVATAR_USERDETAILS")
   public static WebElement  profileIcon;



	public HeaderPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}

	public void logout() throws InterruptedException {
		waitForElementToDisplay(profileIcon);
		clickElement(profileIcon);
		waitForElementToDisplay(logout);
		clickElement(logout);

	}

	

}
