package pages.SalesForce;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.aventstack.extentreports.ExtentTest;

import static driverfactory.Driver.*;
import static verify.SoftAssertions.*;

public class SF_Dashboard {
	
	String dashboardUserName= "Sindhu";
	
	@FindBy(xpath="(//iframe[contains(@id, 'ext-comp')])[1]")
	public static WebElement dashboardiFrame;
	@FindBy(xpath="//span[@class='pageType']/h1")
	public static WebElement dashboardTitleText;
	
	public SF_Dashboard(WebDriver driver){
		PageFactory.initElements(driver, this);
	}
	
	public void checkDashboardElements(WebDriver driver, ExtentTest test) throws InterruptedException{
		Thread.sleep(3000);
		switchToFrame(driver, dashboardiFrame);
		waitForElementToDisplay(dashboardTitleText);
		verifyElementTextContains(dashboardTitleText, dashboardUserName, test);
		switchToDefaultContent(driver);
	}
}
