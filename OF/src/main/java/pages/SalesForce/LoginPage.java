package pages.SalesForce;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import static driverfactory.Driver.waitForElementToDisplay;
import static driverfactory.Driver.clickElement;
import static driverfactory.Driver.setInput;

public class LoginPage {

	@FindBy(xpath = "//button[text()='Sign In']")
	public static WebElement loginBtn;
	@FindBy(id = "txtEmail")
	public static WebElement emailInput;
	
	@FindBy(css = "input[id='txtPassword-clone']")
	public static WebElement password;
	@FindBy(css = "input[id='txtPassword']")
	public static WebElement password1;
	
	
	@FindBy(css = "button[type='submit']")
	public static WebElement submit;
	
	@FindBy(id = "ContentPlaceHolder1_PassiveSignInButton")
	public static WebElement nextBtn;
	
	
	@FindBy(css = "button[title='Enter']")
	public static WebElement enterBtn;
	
	public LoginPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}
public void login(String uname, String pwd) throws InterruptedException
{
	//Thread.sleep(8000);

	waitForElementToDisplay(loginBtn);
	clickElement(loginBtn);
	waitForElementToDisplay(nextBtn);
	clickElement(nextBtn);
	setInput(emailInput,uname);
	clickElement(password);
	setInput(password1,pwd);
	waitForElementToDisplay(enterBtn);
	clickElement(enterBtn);
	//Thread.sleep(8000);
}
}
