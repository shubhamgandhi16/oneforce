package pages.SalesForce;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import static driverfactory.Driver.*;
import static pages.SalesForce.SF_ContactsPage.*;

import java.util.List;

public class SF_Header {
	String seachSF= "VB"; 
	public static String searchExistingContact ="Automation Tester";
	//public static String searchExistingContact ="Shubham+1+1 Gandhi+1";
	
	@FindBy(xpath="//img[@title='Console']")
	public static WebElement salesForceLogo;
	@FindBy(xpath="//button[@id='ext-gen33']")
	public static WebElement dropdownMenuButton;
	@FindBy(id="phSearchInput")
	public static WebElement searchSalesForce;
	@FindBy(id="phSearchInput_autoCompleteBoxId")
	public static WebElement salesForceSearchResults;
	@FindBy(css="a[title='Help & Training (New Window)']")
	public static WebElement helpAndTrainingIcon;
	@FindBy(css="span[id='userNavLabel']")
	public static WebElement userMenuIcon;
	@FindBy(xpath="//*[@id='userNavMenu']")
	public static WebElement userMenuOpen;
	@FindBy(xpath="//a[@title='My Settings']")
	public static WebElement userMenu_MySettings;
	@FindBy(id="app_logout")
	public static WebElement userMenu_Logout;
	@FindBy(xpath="//*[@id='ptBody']//h1")
	public static WebElement userHeading;
	@FindBy(id="ext-gen35")
	public static WebElement addTab;
	
	@FindBy(css="#x-menu-el-nav-tab-0 a")
	public static WebElement selectHome;
	@FindBy(css="#x-menu-el-nav-tab-1 a")
	public static WebElement selectAccounts;
	@FindBy(xpath="//span[contains(text(), 'Contacts')]")
	public static WebElement selectContacts;
	@FindBy(css="#x-menu-el-nav-tab-3 a")
	public static WebElement selectCases;
	@FindBy(css="#x-menu-el-nav-tab-4 a")
	public static WebElement selectOpportunities;
	@FindBy(css="#x-menu-el-nav-tab-5 a")
	public static WebElement selectReports;
	@FindBy(css="#x-menu-el-nav-tab-6 a")
	public static WebElement selectDashboards;
	
	@FindBy(xpath="//ul[@id='ext-gen27']//li[2]")
	public static WebElement firstTab;
	@FindBy(xpath="//ul[@id='ext-gen27']//li[contains(@class, 'x-tab-strip')]")
	public static WebElement allTabsCount;
	@FindBy(xpath="//ul[@id='ext-gen27']//li[2]/a[1]")
	public static WebElement firstTabCloseIcon;
	
	public SF_Header(WebDriver driver){
		PageFactory.initElements(driver, this);
	}
	
	public void checkHeaderElements() throws InterruptedException{
		waitForElementToDisplay(salesForceLogo);
		waitForElementToDisplay(dropdownMenuButton);
		waitForElementToDisplay(searchSalesForce);
		setInput(searchSalesForce, seachSF);
		waitForElementToDisplay(salesForceSearchResults);
		waitForElementToDisplay(helpAndTrainingIcon);
		waitForElementToDisplay(userMenuIcon);
		clickElement(userMenuIcon);
		waitForElementToDisplay(userMenuOpen);
		waitForElementToDisplay(userMenu_MySettings);
		waitForElementToDisplay(userMenu_Logout);
		waitForElementToDisplay(addTab);
	}
	
	public void selectAccountsFromDropdown(WebDriver driver) throws InterruptedException{
		waitForElementToDisplay(dropdownMenuButton);
		clickPseudoElementByOffset(driver, dropdownMenuButton, 127);
		Thread.sleep(3000);
		clickElementUsingJavaScript(driver, selectAccounts);
	}
	
	public void selectHomeFromDropdown(WebDriver driver) throws InterruptedException{
		waitForElementToDisplay(dropdownMenuButton);
		clickPseudoElementByOffset(driver, dropdownMenuButton, 127);
		Thread.sleep(3000);
		clickElementUsingJavaScript(driver, selectHome);
	}
	
	public void selectContactsFromDropdown(WebDriver driver) throws InterruptedException{
		waitForElementToDisplay(dropdownMenuButton);
		clickPseudoElementByOffset(driver, dropdownMenuButton, 127);
		Thread.sleep(3000);
		clickElementUsingJavaScript(driver, selectContacts);
	}
	
	public void selectCasesFromDropdown(WebDriver driver) throws InterruptedException{
		waitForElementToDisplay(dropdownMenuButton);
		clickPseudoElementByOffset(driver, dropdownMenuButton, 127);
		Thread.sleep(3000);
		clickElementUsingJavaScript(driver, selectCases);
	}
	
	public void selectOpportunitiesFromDropdown(WebDriver driver) throws InterruptedException{
		waitForElementToDisplay(dropdownMenuButton);
		clickPseudoElementByOffset(driver, dropdownMenuButton, 127);
		Thread.sleep(3000);
		clickElementUsingJavaScript(driver, selectOpportunities);
	}
	
	public void selectReportsFromDropdown(WebDriver driver) throws InterruptedException{
		waitForElementToDisplay(dropdownMenuButton);
		clickPseudoElementByOffset(driver, dropdownMenuButton, 127);
		Thread.sleep(3000);
		clickElementUsingJavaScript(driver, selectReports);
	}
	
	public void selectDashboardsFromDropdown(WebDriver driver) throws InterruptedException{
		waitForElementToDisplay(dropdownMenuButton);
		clickPseudoElementByOffset(driver, dropdownMenuButton, 127);
		Thread.sleep(3000);
		clickElementUsingJavaScript(driver, selectDashboards);
	}
	
	public void closeFirstTab(WebDriver driver) throws InterruptedException{
		hoverOverElement(driver, firstTab);
		clickElement(firstTabCloseIcon);
	}
	
	public void closeAllTabs(WebDriver driver) throws InterruptedException{
		List<WebElement> tabs = driver.findElements(By.xpath("//ul[@id='ext-gen27']//li[contains(@class, 'x-tab-strip-closable')]"));
		System.out.println("Number of Tabs: "+tabs.size());
		for (int i=1; i<=tabs.size(); i++){
			hoverOverElement(driver, firstTab);
			clickElement(firstTabCloseIcon);
		}
	}
	
	public void searchingAnAccount(){
		waitForElementToDisplay(searchSalesForce);
		setInput(searchSalesForce, accountName);
		sendKeysENTER(searchSalesForce);
	}
	
	public void searchAContact(){
		waitForElementToDisplay(searchSalesForce);
		setInput(searchSalesForce, searchFirstAndLastname);
		sendKeysENTER(searchSalesForce);
	}
	
	public void searchExistingContact(){
		waitForElementToDisplay(searchSalesForce);
		setInput(searchSalesForce, searchExistingContact);
		sendKeysENTER(searchSalesForce);
	}
}
