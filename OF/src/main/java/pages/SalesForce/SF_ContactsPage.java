package pages.SalesForce;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.aventstack.extentreports.ExtentTest;

import static driverfactory.Driver.*;
import static pages.SalesForce.SF_Header.*;
import static verify.SoftAssertions.*;

import java.util.Set;

public class SF_ContactsPage {
	static String timestamp= getCurrTimeStamp();
	
	String contactType ="My Contacts";
	String recordType= "VB";
	
	static String salutation= "Ms.";
	static String firstName= "Sindhu";
	static String lastName= "Ganesan"+timestamp;
	static String expectedPageDescription = salutation+" "+firstName+" "+lastName;
	static String searchFirstAndLastname = firstName+" "+lastName;
	
	static String editFirstName = firstName+"AE";
	static String searchEditedFirstAndLastName = editFirstName+" "+lastName;
	static String editedContactName = salutation+" "+editFirstName+" "+lastName;
	
	static String accountName= "AutomationTest Account";
	//String accountName= "21st Century Fox";
	String legalStreet= "Hinjewadi";
	String legalCity= "Pune";
	String legalStateCode= "Maharashtra";
	String legalPostalCode= "411047";
	String legalCountryCode= "India";
	String SSN= "333-22-4444";
	
	String expectedEditTitle= "Contact Edit";
	
	String certificateName="Certificate 1";
	String billingName="UB-0003873960";
	String eligibilityName="EL-46391170";
	String noteName="test";
	String formName="sample.docx";
	
	String firstContName;
	
	//New Opportunity
	static String opportunityName = "AutoOpportunity"+timestamp;
	String responseType = "Home Mailer";
	String product = "Home"; 
	String stage = "1-Eligibility Verification";
	String eligible = "Yes";
	
	//New Disposition or Case
	String category="Inquiries";
	String subCategory="Ancillary";
	String productCase="Personal Lines";
	String routeCase="Yes";
	String carriers="Chubb Marine";
	String primaryPhone="897898992";
	String commentOnDisposition="testCases"+timestamp;
	String preferredEmail="testCase@gmail.com"; 
	
	//New Workpiece
	String productsWorkpiece= "Personal Lines";
	String routedToQueue= "VB - BC";
	String routedToUser= "Poorani Nagarajan BA";
	String primaryPhoneWorkpiece= "89897281283"; 
	String workpieceComments= "CreatedWorkpiece"+timestamp;			
	
	//Contacts Home
	@FindBy(id="ext-comp-1005")
	public static WebElement contactsHomeiFrame;
	@FindBy(name="newContact")
	public static WebElement newContactBtn;
	@FindBy(xpath="//select[contains(@id, 'listSelect')]")
	public static WebElement contactsSelect;
	@FindBy(xpath="//tbody//td[contains(@class, 'FULL_NAME')]//a[1]")
	public static WebElement firstContactName;
	@FindBy(xpath="//*[@title='Account Name']")
	public static WebElement accountNameTableHeader;
	
	//Contact Details
	@FindBy(xpath="(//iframe[contains(@id, 'ext-comp')])[2]")
	public static WebElement contactDetailsiFrame;
	@FindBy(xpath="//iframe[@title='Activity_History']")
	public static WebElement activityHistoryiFrame;
	@FindBy(xpath="//img[@title='Contact']/following::h2[@class='pageDescription']")
	public static WebElement pageDescription;
	@FindBy(xpath="//input[@title='Edit']")
	public static WebElement contactDetail_EditBtn;
	@FindBy(xpath="//*[@class='listTitle'][contains(text(), 'Eligibility')]")
	public static WebElement contactDetail_Eligibility;
	@FindBy(xpath="//img[@title='Eligibility']")
	public static WebElement contactDetail_EligibilityDetails;
	@FindBy(xpath="//img[@title='Eligibility']/following::table//a")
	public static WebElement contactDetail_EligibilityName;
	
	@FindBy(xpath="//*[@class='listTitle'][contains(text(), 'Opportunities')]")
	public static WebElement contactDetail_Opportunities;
	@FindBy(css="input[title='New Opportunity']")
	public static WebElement contactDetail_NewOpportunityBtn;
	@FindBy(xpath="//*[contains(@class, 'mainTitle')][text()='Activity History']")
	public static WebElement activityHistoryHeader;
	@FindBy(xpath="//*[@class='mainTitle'][contains(text(), 'Activity History')]/following::td[contains(@id, 'CustomList')]/a")
	public static WebElement activityHistory_recentActivity;
	
	
	@FindBy(xpath="//*[@class='listTitle'][contains(text(), 'Certificates')]")
	public static WebElement contactDetail_Certificates;
	@FindBy(xpath="//img[@title='Certificate']")
	public static WebElement contactDetail_CertificateDetails;
	@FindBy(xpath="//img[@title='Certificate']/following::table//a")
	public static WebElement contactDetail_CertificateName;
	
	@FindBy(xpath="//*[@class='listTitle'][contains(text(), 'Billings')]")
	public static WebElement contactDetail_Billings;
	@FindBy(xpath="//img[@title='Billing']")
	public static WebElement contactDetail_BillingDetails;
	@FindBy(xpath="//img[@title='Billing']/following::table//a")
	public static WebElement contactDetail_BillingName;
	
	@FindBy(xpath="//*[@class='listTitle'][contains(text(), 'Notes')]")
	public static WebElement contactDetail_Notes;
	@FindBy(xpath="//*[contains(@id, 'title')][text()='Notes']")
	public static WebElement contactDetail_NotesDetails;
	@FindBy(xpath="//*[contains(@id, 'title')][text()='Notes']/following::table//a")
	public static WebElement contactDetail_NoteName;
	
	@FindBy(xpath="//*[@class='listTitle'][contains(text(), 'Forms')]")
	public static WebElement contactDetail_Forms;
	@FindBy(xpath="//*[contains(@id, 'title')][text()='Forms']")
	public static WebElement contactDetail_formDetails;
	@FindBy(xpath="//*[contains(@id, 'title')][text()='Forms']/following::table//a")
	public static WebElement contactDetail_formName;
	
	//View Claims
	@FindBy(xpath="//input[@value='View Claims']")
	public static WebElement contactDetail_viewClaimsBtn;
	@FindBy(xpath="//*[@class='mainTitle'][contains(text(), 'Certificates')]")
	public static WebElement contactDetail_viewClaimsWindow_certificatesTitle;
	@FindBy(xpath="//*[@class='mainTitle'][contains(text(), 'Claims')]")
	public static WebElement contactDetail_viewClaimsWindow_claimTitle;
	@FindBy(xpath="//*[@class='mainTitle'][contains(text(), 'Certificates')]/following::td/span")
	public static WebElement contactDetail_viewClaimsWindow_certificateName;
	
	//Creating Disposition
	@FindBy(xpath="//input[@value='Select Disposition']")
	public static WebElement contactDetail_selectDispositionBtn;
	@FindBy(xpath="(//iframe[contains(@id, 'ext-comp')])[3]")
	public static WebElement createDispositioniFrame;
	@FindBy(xpath="//select[contains(@id, 'category')]")
	public static WebElement createDisposition_selectCategory;
	@FindBy(xpath="//select[contains(@id, 'subcategory')]")
	public static WebElement createDisposition_selectSubCategory;
	@FindBy(xpath="//select[contains(@id, 'product')]")
	public static WebElement createDisposition_selectProduct;
	@FindBy(xpath="//select[contains(@id, 'Routing')]")
	public static WebElement createDisposition_selectRouteCase;
	@FindBy(xpath="//input[contains(@id, 'slctdCert')]")
	public static WebElement createDisposition_checkExistingCertificate;
	@FindBy(xpath="//*[contains(text(), 'Carriers')]/following::select[1]")
	public static WebElement createDisposition_selectCarriers;
	@FindBy(xpath="//input[contains(@id, 'phone')]")
	public static WebElement createDisposition_primaryPhoneInput;
	@FindBy(xpath="//textarea[contains(@id, 'comment')]")
	public static WebElement createDisposition_commentTextArea;
	@FindBy(xpath="//input[contains(@id, 'preferredemail')]")
	public static WebElement createDisposition_preferredEmail;
	@FindBy(xpath="//input[@value='Submit Dispositions']")
	public static WebElement createDisposition_submitDispositions;
	
	
	@FindBy(xpath="(//iframe[contains(@id, 'ext-comp')])[3]")
	public static WebElement certificateDetailsiFrame;
	@FindBy(xpath="//img[@title='Certificate']/following::*[@class='pageDescription']")
	public static WebElement certificateDetails_CertificateName;
	@FindBy(xpath="//td[text()='Certificate Name']/following::td[1]")
	public static WebElement certificateDetails_CertificateNameNotEditable;
	@FindBy(xpath="//*[@id='ext-comp-1018']//li[2]/following::span[@class='tabText'][2]")
	public static WebElement certificateDetails_CertificateTab;
	
	@FindBy(xpath="(//iframe[contains(@id, 'ext-comp')])[3]")
	public static WebElement eligibilityDetailsiFrame;
	@FindBy(xpath="//img[@title='Eligibility']/following::*[@class='pageDescription']")
	public static WebElement eligibilityDetails_eligibilityName;
	@FindBy(xpath="//td[text()='Eligibility Name']/following::td[1]")
	public static WebElement eligibilityDetails_eligibilityNameNotEditable;
	
	@FindBy(xpath="(//iframe[contains(@id, 'ext-comp')])[3]")
	public static WebElement billingDetailsiFrame;
	@FindBy(xpath="//img[@title='Billing']/following::*[@class='pageDescription']")
	public static WebElement billingDetails_billingName;
	@FindBy(xpath="//td[text()='UBS Billing Name']/following::td[1]")
	public static WebElement billingDetails_billingNameNotEditable;

	@FindBy(xpath="(//iframe[contains(@id, 'ext-comp')])[3]")
	public static WebElement noteDetailsiFrame;
	@FindBy(xpath="//*[@title='Custom']/following::*[@class='pageDescription']")
	public static WebElement noteDetails_noteName;
	@FindBy(xpath="//td[text()='Note Name']/following::td[1]")
	public static WebElement noteDetails_noteNameNotEditable;
	
	@FindBy(xpath="(//iframe[contains(@id, 'ext-comp')])[3]")
	public static WebElement formDetailsiFrame;
	@FindBy(xpath="//*[@title='Custom']/following::*[@class='pageDescription']")
	public static WebElement formDetails_formName;
	
	@FindBy(xpath="(//iframe[contains(@id, 'ext-comp')])[3]")
	public static WebElement createOpportunityiFrame;
	@FindBy(className="pageDescription")
	public static WebElement opportunityPageDescription;
	@FindBy(xpath="//label[contains(text(), 'Opportunity Name')]/following::input[1]")
	public static WebElement createOpportunity_OpportunityName;
	@FindBy(xpath="//label[contains(text(), 'Response Type')]/following::select[1]")
	public static WebElement createOpportunity_ResponseType;
	@FindBy(xpath="//label[contains(text(), 'Product')]/following::select[1]")
	public static WebElement createOpportunity_Product;
	@FindBy(xpath="//label[contains(text(), 'Contact')]/following::input[@id='CF00N1J00000F3tx1']")
	public static WebElement createOpportunity_Contact;
	@FindBy(xpath="//label[contains(text(), 'Stage')]/following::select[1]")
	public static WebElement createOpportunity_Stage;
	@FindBy(xpath="//label[contains(text(), 'Actual Closed Date')]/following::input[1]")
	public static WebElement createOpportunity_ClosedDate;
	@FindBy(xpath="//label[contains(text(), 'Eligible')]/following::select[1]")
	public static WebElement createOpportunity_Eligible;
	@FindBy(xpath="//label[contains(text(), 'Date eligibility response received')]/following::input[1]")
	public static WebElement createOpportunity_DateEligibilityResponse;
	@FindBy(xpath="//label[contains(text(), 'Target Closed Date')]/following::input[1]")
	public static WebElement createOpportunity_TargetClosedDate;
	@FindBy(xpath="//input[@name='save']")
	public static WebElement createOpportunity_SaveBtn;
	@FindBy(xpath="//img[@title='Opportunity']/following::*[@class='pageDescription']")
	public static WebElement createOpportunity_createdPageDescription;
	
	
	@FindBy(xpath="(//iframe[contains(@id, 'ext-comp')])[2]")
	public static WebElement contactRecordTypeSelectiFrame;
	@FindBy(id="p3")
	public static WebElement contactRecordTypeSelect;
	@FindBy(xpath="//input[@value='Continue']")
	public static WebElement continueRTBtn;
	
	@FindBy(xpath="(//iframe[contains(@id, 'ext-comp')])[2]")
	public static WebElement newContactFormiFrame;
	@FindBy(xpath="//label[contains(text(), 'Salutation')]/following::select[1]")
	public static WebElement salutationSelect;
	@FindBy(xpath="//label[contains(text(), 'First Name')]/following::input[1]")
	public static WebElement firstnameInput;
	@FindBy(xpath="//label[contains(text(), 'Last Name')]/following::input[1]")
	public static WebElement lastnameInput;
	@FindBy(xpath="//label[contains(text(), 'Account Name')]//following::span/input[1]")
	public static WebElement accountnameInput;
	@FindBy(xpath="//label[contains(text(), 'Legal Street')]/following::textarea[1]")
	public static WebElement legalStreetInput;
	@FindBy(xpath="//label[contains(text(), 'Legal City')]/following::input[1]")
	public static WebElement legalCityInput;
	@FindBy(xpath="//label[contains(text(), 'Legal State')]/following::select[1]")
	public static WebElement legalStateSelect;
	@FindBy(xpath="//label[contains(text(), 'Legal Zip')]/following::input[1]")
	public static WebElement legalZipCodeInput;
	@FindBy(xpath="//label[contains(text(), 'Legal Country')]/following::select[1]")
	public static WebElement legalCountrySelect;
	@FindBy(xpath="//label[contains(text(), 'SSN')]/following::input[1]")
	public static WebElement SSNInput;
	@FindBy(xpath="//label[contains(text(), 'DOB')]/following::input[1]")
	public static WebElement DOBInput;
	@FindBy(xpath="//select[@id='calMonthPicker']/following::img[1]")
	public static WebElement monthPickerNext;
	@FindBy(xpath="//*[@class='weekday'][contains(text(), '15')]")
	public static WebElement datePicker15;
	@FindBy(css="input[value='Save']")
	public static WebElement saveContactBtn;
	@FindBy(css="input[value='Cancel']")
	public static WebElement cancelContactBtn;
	@FindBy(css="a[id='ext-gen221']")
	public static WebElement closeContactTabBtn;

	@FindBy(xpath="(//iframe[contains(@id, 'ext-comp')])[2]")
	public static WebElement editContactFormiFrame;
	@FindBy(xpath="//*[@class='mainTitle']")
	public static WebElement editContactTitle;
	@FindBy(xpath="//label[contains(text(), 'First Name')]/following::input[1]")
	public static WebElement editContactFirstnameInput;
	@FindBy(css="input[value='Save']")
	public static WebElement editContactSaveButton;
	
	//Create Workpiece
	@FindBy(xpath="//input[@value='Create WorkPiece']")
	public static WebElement contactDetail_createWorkplaceBtn;
	@FindBy(xpath="(//iframe[contains(@id, 'ext-comp')])[3]")
	public static WebElement createWorkplaceiFrame;
	@FindBy(id="thePage:form1:block1:theTable:0:selectedid")
	public static WebElement createWorkplace_selectDispositionCheckbox;
	@FindBy(id="thePage:form1:block1:theTable:0:productsel")
	public static WebElement createWorkplace_selectProducts;
	@FindBy(name="thePage:form1:block1:theTable:0:j_id40")
	public static WebElement createWorkplace_selectRoutedToQueue;
	@FindBy(id="thePage:form1:block1:theTable:0:selectuser")
	public static WebElement createWorkplace_selectRoutedToUser;
	@FindBy(id="thePage:form1:titlesec:bphone")
	public static WebElement createWorkplace_primaryPhoneInput;
	@FindBy(id="thePage:form1:titlesec:comment")
	public static WebElement createWorkplace_commentsInput;
	@FindBy(xpath="//input[@value='Submit Dispositions']")
	public static WebElement createWorkplace_submitDispositionBtn;
	@FindBy(xpath="//iframe[@title='CaseRelatedListPage']")
	public static WebElement caseRelatediframe;
	@FindBy(xpath="//h2[text()='Related Cases']/following::table//td[2]/a")
	public static WebElement viewContact_firstRelatedCase;
	
	@FindBy(xpath="(//iframe[contains(@id, 'ext-comp')])[3]")
	public static WebElement viewCaseiFrame;
	@FindBy(xpath="//iframe[@title='ExtractDispoCommentPage_ACTIVE']")
	public static WebElement viewCase_commentsiFrame;
	@FindBy(xpath="//textarea")
	public static WebElement viewCase_commentsTextArea;
	
	public SF_ContactsPage(WebDriver driver) {
		PageFactory.initElements(driver,  this);
	}
	
	public void testContactElements(WebDriver driver) throws InterruptedException{
		switchToFrame(driver, contactsHomeiFrame);
		waitForElementToDisplay(newContactBtn);
		waitForElementToDisplay(contactsSelect);
		waitForElementToDisplay(accountNameTableHeader);
		selEleByVisbleText(contactsSelect, contactType);
		switchToDefaultContent(driver);
	}
	
	public void selectExistingFirstContact(WebDriver driver) throws InterruptedException{
		switchToFrame(driver, contactsHomeiFrame);
		clickElement(firstContactName);
		switchToDefaultContent(driver);
	}
	
	public void selectNewOpportunity(WebDriver driver) throws InterruptedException{
		switchToFrame(driver, contactDetailsiFrame);
		firstContName= getTextOfWebElement(pageDescription);
		firstContName= firstContName.substring(4, firstContName.length()).trim();

		clickElement(contactDetail_Opportunities);
		clickElement(contactDetail_NewOpportunityBtn);
		switchToDefaultContent(driver);
	}
	
	public void createNewOpportunityAndSave(WebDriver driver) throws InterruptedException{
		switchToFrame(driver, createOpportunityiFrame);
		waitForElementToDisplay(opportunityPageDescription);
		setInput(createOpportunity_OpportunityName, opportunityName);
		selEleByVisbleText(createOpportunity_ResponseType, responseType);
		selEleByVisbleText(createOpportunity_Product, product);
		selEleByVisbleText(createOpportunity_Stage, stage);
		clickElement(createOpportunity_ClosedDate);
		clickElement(monthPickerNext);
		clickElement(datePicker15);
		selEleByVisbleText(createOpportunity_Eligible, eligible);
		clickElement(createOpportunity_DateEligibilityResponse);
		clickElement(monthPickerNext);
		clickElement(datePicker15);
		clickElement(createOpportunity_TargetClosedDate);
		clickElement(monthPickerNext);
		clickElement(datePicker15);
		clickElement(createOpportunity_SaveBtn);
		switchToDefaultContent(driver);
	}
	
	public void checkNewOpportunityTitleAfterCreating(WebDriver driver, ExtentTest test) throws InterruptedException{
		switchToFrame(driver, createOpportunityiFrame);
		verifyElementText(createOpportunity_createdPageDescription, opportunityName, test);
		switchToDefaultContent(driver);
	}
	
	public void searchCreatedOpportunity(){
		waitForElementToDisplay(searchSalesForce);
		setInput(searchSalesForce, opportunityName);
		sendKeysENTER(searchSalesForce);
	}
	
	public void clickNewContactBtn(WebDriver driver) throws InterruptedException{
		switchToFrame(driver, contactsHomeiFrame);
		clickElement(newContactBtn);
		switchToDefaultContent(driver);
	}
	
	public void selectContactRecordType(WebDriver driver) throws InterruptedException{
		switchToFrame(driver, contactRecordTypeSelectiFrame);
		waitForElementToDisplay(contactRecordTypeSelect);
		selEleByVisbleText(contactRecordTypeSelect, recordType);
		clickElement(continueRTBtn);
		switchToDefaultContent(driver);
	}
	
	public void fillRequiredContactFormFieldsAndSave(WebDriver driver) throws InterruptedException{
		switchToFrame(driver, newContactFormiFrame);
		waitForElementToDisplay(salutationSelect);
		selEleByVisbleText(salutationSelect, salutation);
		waitForElementToDisplay(firstnameInput);
		setInput(firstnameInput, firstName);
		waitForElementToDisplay(lastnameInput);
		setInput(lastnameInput, lastName);
		waitForElementToDisplay(accountnameInput);
		setInput(accountnameInput, accountName);
		waitForElementToDisplay(legalStreetInput);
		setInput(legalStreetInput, legalStreet);
		waitForElementToDisplay(legalCityInput);
		setInput(legalCityInput, legalCity);
		waitForElementToDisplay(legalCountrySelect);
		selEleByVisbleText(legalCountrySelect, legalCountryCode);
		waitForElementToDisplay(legalStateSelect);
		selEleByVisbleText(legalStateSelect, legalStateCode);
		waitForElementToDisplay(legalZipCodeInput);
		setInput(legalZipCodeInput, legalPostalCode);
		waitForElementToDisplay(SSNInput);
		setInput(SSNInput, SSN);
		waitForElementToDisplay(saveContactBtn);
		waitForElementToDisplay(cancelContactBtn);
		waitForElementToDisplay(DOBInput);
		clickElement(DOBInput);
		clickElement(monthPickerNext);
		clickElement(datePicker15);
	
		clickElement(saveContactBtn);
		switchToDefaultContent(driver);
	}
	
	public void testCreatedContact(WebDriver driver, ExtentTest test) throws InterruptedException{
		Thread.sleep(2000);
		switchToFrame(driver, contactDetailsiFrame);	
		verifyElementTextContains(pageDescription, expectedPageDescription, test);
		switchToDefaultContent(driver);
	}
	
	public void clickEditOfContact(WebDriver driver) throws InterruptedException{
		switchToFrame(driver, contactDetailsiFrame);
		clickElement(contactDetail_EditBtn);
		switchToDefaultContent(driver);
	}
	
	public void editContactAndSave(WebDriver driver, ExtentTest test) throws InterruptedException{
		switchToFrame(driver, editContactFormiFrame);
		verifyElementText(editContactTitle, expectedEditTitle, test);
		setInput(editContactFirstnameInput, editFirstName);
		clickElement(editContactSaveButton);
		switchToDefaultContent(driver);
	}
	
	public void searchAndSelectEditedContact(){
		waitForElementToDisplay(searchSalesForce);
		setInput(searchSalesForce, searchEditedFirstAndLastName);
		sendKeysENTER(searchSalesForce);
	}
	
	public void testEligibilityDetails(WebDriver driver, ExtentTest test) throws InterruptedException{	
		switchToFrame(driver, contactDetailsiFrame);
		waitForElementToDisplay(contactDetail_Eligibility);
		clickElement(contactDetail_Eligibility);
		waitForElementToDisplay(contactDetail_EligibilityDetails);
		clickElement(contactDetail_EligibilityName);
		switchToDefaultContent(driver);
		
		switchToFrame(driver, eligibilityDetailsiFrame);
		verifyElementText(eligibilityDetails_eligibilityName, eligibilityName, test);
		clickElement(eligibilityDetails_eligibilityNameNotEditable);
		
		verifyNotEditable(eligibilityDetails_eligibilityNameNotEditable, test);	
		switchToDefaultContent(driver);
	}
	
	public void checkTheCertificate(WebDriver driver, ExtentTest test) throws InterruptedException{		
		switchToFrame(driver, contactDetailsiFrame);
		waitForElementToDisplay(contactDetail_Certificates);
		clickElement(contactDetail_Certificates);
		waitForElementToDisplay(contactDetail_CertificateDetails);
		clickElement(contactDetail_CertificateName);
		switchToDefaultContent(driver);
		
		switchToFrame(driver, certificateDetailsiFrame);
		verifyElementText(certificateDetails_CertificateName, certificateName, test);
		clickElement(certificateDetails_CertificateNameNotEditable);
		
		verifyNotEditable(certificateDetails_CertificateNameNotEditable, test);	
		switchToDefaultContent(driver);
	}
	
	public void checkTheBillingDetails(WebDriver driver, ExtentTest test) throws InterruptedException{		
		switchToFrame(driver, contactDetailsiFrame);
		waitForElementToDisplay(contactDetail_Billings);
		clickElement(contactDetail_Billings);
		waitForElementToDisplay(contactDetail_BillingDetails);
		clickElement(contactDetail_BillingName);
		switchToDefaultContent(driver);
		
		switchToFrame(driver, billingDetailsiFrame);
		verifyElementText(billingDetails_billingName, billingName, test);
		clickElement(billingDetails_billingNameNotEditable);
		
		verifyNotEditable(billingDetails_billingNameNotEditable, test);
		switchToDefaultContent(driver);
	}
	
	public void checkTheClaims(WebDriver driver, ExtentTest test) throws InterruptedException{		
		switchToFrame(driver, contactDetailsiFrame);
		waitForElementToDisplay(contactDetail_viewClaimsBtn);
		String parentwH = driver.getWindowHandle();
		clickElement(contactDetail_viewClaimsBtn);
		switchToDefaultContent(driver);
		
		Set <String> wH = driver.getWindowHandles();
		System.out.println(wH.size());
		for(String str : wH) {
			if(!(driver.switchTo().window(str).getWindowHandle().equals(parentwH))) {
				
				driver.switchTo().window(str);
				waitForElementToDisplay(contactDetail_viewClaimsWindow_certificatesTitle);
				verifyElementText(contactDetail_viewClaimsWindow_certificateName, certificateName, test);
				waitForElementToDisplay(contactDetail_viewClaimsWindow_claimTitle);
	
				driver.switchTo().window(str).close();
				//break;
			}
		}
		driver.switchTo().window(parentwH);		
	}
	
	public void checkTheNotes(WebDriver driver, ExtentTest test) throws InterruptedException{		
		switchToFrame(driver, contactDetailsiFrame);
		waitForElementToDisplay(contactDetail_Notes);
		clickElement(contactDetail_Notes);
		waitForElementToDisplay(contactDetail_NotesDetails);
		clickElement(contactDetail_NoteName);
		switchToDefaultContent(driver);
		
		switchToFrame(driver, noteDetailsiFrame);
		verifyElementText(noteDetails_noteName, noteName, test);
		clickElement(noteDetails_noteName);
		switchToDefaultContent(driver);		
	}
	
	public void checkTheForms(WebDriver driver, ExtentTest test) throws InterruptedException{		
		switchToFrame(driver, contactDetailsiFrame);
		waitForElementToDisplay(contactDetail_Forms);
		clickElement(contactDetail_Forms);
		waitForElementToDisplay(contactDetail_formDetails);
		clickElement(contactDetail_formName);
		switchToDefaultContent(driver);
		
		switchToFrame(driver, formDetailsiFrame);
		verifyElementText(formDetails_formName, formName, test);
		switchToDefaultContent(driver);		
	}
	
	public void selectDisposition(WebDriver driver) throws InterruptedException{
		switchToFrame(driver, contactDetailsiFrame);
		clickElement(contactDetail_selectDispositionBtn);
		switchToDefaultContent(driver);
	}
	
	public void createDispositionAndSave(WebDriver driver) throws InterruptedException{
		switchToFrame(driver, createDispositioniFrame);
		selEleByVisbleText(createDisposition_selectCategory, category);
		selEleByVisbleText(createDisposition_selectSubCategory, subCategory);
		selEleByVisbleText(createDisposition_selectProduct, productCase);
		selEleByVisbleText(createDisposition_selectRouteCase, routeCase);
		clickElement(createDisposition_checkExistingCertificate);
		selEleByVisbleText(createDisposition_selectCarriers, carriers);
		setInput(createDisposition_primaryPhoneInput, primaryPhone);
		setInput(createDisposition_commentTextArea, commentOnDisposition);
		setInput(createDisposition_preferredEmail, preferredEmail);
		clickElement(createDisposition_submitDispositions);
		switchToDefaultContent(driver);
	}
	
	public void testActivityHistory(WebDriver driver, ExtentTest test) throws InterruptedException{
		Thread.sleep(2000);
		switchToFrame(driver, contactDetailsiFrame);
		switchToFrame(driver, activityHistoryiFrame);
		waitForElementToDisplay(activityHistoryHeader);
		waitForElementToDisplay(activityHistory_recentActivity);
		verifyElementText(activityHistory_recentActivity, category, test);
		switchToDefaultContent(driver);
	}
	
	public void clickCreateWorkplace(WebDriver driver) throws InterruptedException{
		switchToFrame(driver, contactDetailsiFrame);
		clickElement(contactDetail_createWorkplaceBtn);
		switchToDefaultContent(driver);
	}
	
	public void createWorkplaceAndSave(WebDriver driver) throws InterruptedException{
		switchToFrame(driver, createWorkplaceiFrame);
		clickElement(createWorkplace_selectDispositionCheckbox);
		selEleByVisbleText(createWorkplace_selectProducts, productsWorkpiece);
		selEleByVisbleText(createWorkplace_selectRoutedToQueue, routedToQueue);
		Thread.sleep(2000);
		selEleByVisbleText(createWorkplace_selectRoutedToUser, routedToUser);
		setInput(createWorkplace_primaryPhoneInput, primaryPhoneWorkpiece);
		setInput(createWorkplace_commentsInput, workpieceComments);
		clickElement(createWorkplace_submitDispositionBtn);
		switchToDefaultContent(driver);
	} 
	
	public void clickCreatedWorkpieceFromCases(WebDriver driver) throws InterruptedException{
		Thread.sleep(2000);
		switchToFrame(driver, contactDetailsiFrame);
		switchToFrame(driver, caseRelatediframe);
		waitForElementToDisplay(viewContact_firstRelatedCase);
		clickElement(viewContact_firstRelatedCase);
		switchToDefaultContent(driver);
	}
	
	public void testTheCreatedWorkpiece(WebDriver driver, ExtentTest test) throws InterruptedException{
		Thread.sleep(2000);
		switchToFrame(driver, viewCaseiFrame);
		switchToFrame(driver, viewCase_commentsiFrame);
		waitForElementToDisplay(viewCase_commentsTextArea);
		String commentsActual= getAttributeValue(viewCase_commentsTextArea, "value");
		verifyEquals(commentsActual, workpieceComments, test);
		switchToDefaultContent(driver);
	}
}
