package pages.SalesForce;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;


import static driverfactory.Driver.clickElement;
import static driverfactory.Driver.waitForElementToDisplay;
import static driverfactory.Driver.waitForElementToDisappear;

import java.util.List;

public class DashBoardPage {
    @FindBy(xpath = "//button[text()='Choose Country']")
    public static WebElement countryDropdwnLnk;
    @FindBy(id = "TE_TERMS_CONDITIONS_AGREEMENT_ACCEPT_BUTTON")
    public static WebElement  termsBtn;
    @FindBy(css = "a[id*='TE_SEARCH_SELECTCOUNTRY_DROPDOWN_ITEM']")
    List<WebElement> countryOption;

    @FindBy(css = "a[id*='TE_SEARCH_SELECTCOUNTRY_DROPDOWN_ITEM']")
    WebElement countryOption1;
    @FindBy(css = "button[id='TE_SEARCH_FAMILY_DROPDOWN']")
    WebElement jobFamilyDropdwnLnk;

    @FindBy(xpath = "//a[contains(text(),'Finance')]")
    WebElement jobFamilyOption;

    @FindBy(css = "div[class$='modal__actions'] button")
    WebElement applyFiltBtn;

    @FindBy(css = "span[id*='TE_SEARCHDATATABLE_JOBDESCRIPTION']>p")
    public static WebElement jobDesc;
    @FindBy(css = "span[id*='TE_>p")
    public static WebElement jobDesc1;
    @FindBy(css = "span[id*='TE_SEARCHDATATABLE_JOBCODE']")
   public static WebElement jobCode;
    @FindBy(css = "span[id*='inavlidloc']")
    public static WebElement jobCode1;
    @FindBy(css = "h4[id*='TE_SEARCHDATATABLE_JOBTITLE']")
   public static WebElement  jobTitle;
    
    
    @FindBy(id = "TE_SEARCH_CLEARCOUNTRY_BTN")
    public static WebElement  removCountry;
    
    @FindBy(css ="h6[id*='TE_SEARCHRESULTS_COUNT']")
    public static WebElement  resultTxt;
    
    
    
    @FindBy(id = "headerlogout")
    public static WebElement  logout;
    @FindBy(css = "mercer-avatar#TE_HEADER_AVATAR_USERDETAILS div.mos-c-avatar")
    public static WebElement  profileIcon;
    
   
  By loader=By.cssSelector("mercer-icon[customclass*='animation']");
    public HeaderPage headerpage;

	public DashBoardPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
		headerpage=new HeaderPage(driver);
	}

	public void searchJob(String country) throws InterruptedException {
		//waitForElementToDisplay(termsBtn);
		//clickElement(termsBtn);
		waitForElementToDisappear(loader);
		//waitForElementToDisplay(profileIcon);
		waitForElementToDisplay(countryDropdwnLnk);
		clickElement(countryDropdwnLnk);
		selectCountry(country);
		clickElement(jobFamilyDropdwnLnk);
		clickElement(jobFamilyOption);
		clickElement(applyFiltBtn);

	}

	public void selectCountry(String country) {
		// TODO Auto-generated method stub
		for(WebElement e: countryOption)
		{
			if(e.getText().contains(country))
			{
				e.click();
				break;
			}
		}
	}
	public void removeCountry() throws InterruptedException {
		clickElement(removCountry);
	
	}

	public void logout() throws InterruptedException {
		// TODO Auto-generated method stub
		clickElement(profileIcon);
		clickElement(logout);

	}

}
