package smoke.application2.tests;


import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import com.aventstack.extentreports.ExtentTest;


import atu.testng.reports.ATUReports;
import atu.testng.reports.logging.LogAs;
import atu.testng.selenium.reports.CaptureScreen;
import atu.testng.selenium.reports.CaptureScreen.ScreenshotOf;
import driverfactory.Driver;
import listeners.RetryListener;
import pages.SalesForce.DashBoardPage;
import pages.SalesForce.LoginPage;
import utilities.InitTests;
import utilities.RetryAnalyzer;
import verify.SoftAssertions;

import static driverfactory.Driver.killBrowserExe;
import static driverfactory.Driver.waitForElementToDisplay;
import static utilities.MyExtentReports.reports;
import static verify.SoftAssertions.verifyElementTextContains;

import java.util.concurrent.TimeUnit;

public class TestMarketPricerJobSearch extends InitTests {
	DashBoardPage home;

	@Test(priority = 1, enabled = false)
	public void testSearchJob() throws Exception {
		  WebDriver driver=null;
		  ExtentTest test=null;
		  Driver driverObj=new Driver();
		try {
			test = reports.createTest("testSearchJob");
			test.assignCategory("smoke");
			 WebDriver webdriver=driverObj.initWebDriver( "","CHROME","local", "");
			 driver=driverObj.getEventDriver(webdriver,test);
			 LoginPage loginPage = new LoginPage(driver);
			loginPage.login(USERNAME,PASSWORD);
			home = new DashBoardPage(driver);
			//Thread.sleep(6000);
			home.searchJob("Greece");
			waitForElementToDisplay(DashBoardPage.jobCode);
			verifyElementTextContains(DashBoardPage.jobCode, "FIN",test);
			verifyElementTextContains(DashBoardPage.jobDesc, "Financial",test);
			//verifyElementTextContains(DashBoardPage.jobTitle,"Financial",test);

		} catch (Error e) {
			e.printStackTrace();
			SoftAssertions.fail(e,driverObj.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
			ATUReports.add("testSearchJob()", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
			softAssert.assertAll();

		} catch (Exception e) {
			e.printStackTrace();
			SoftAssertions.fail(e,driverObj.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
			ATUReports.add("testSearchJob()", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
			softAssert.assertAll();
		} 
		finally
		{
			reports.flush();
			driver.close();
			killBrowserExe(BROWSER_TYPE);

		}
	}
	

	
@AfterSuite
public void kill()
{
	//killBrowserExe("chrome");

}
}
