package smoke.application2.tests;


import org.openqa.selenium.WebDriver;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import static driverfactory.Driver.waitForElementToDisplay;
import static utilities.MyExtentReports.reports;

import static verify.SoftAssertions.verifyElementTextContains;
import com.aventstack.extentreports.ExtentTest;

import atu.testng.reports.ATUReports;
import atu.testng.reports.logging.LogAs;
import atu.testng.selenium.reports.CaptureScreen;
import atu.testng.selenium.reports.CaptureScreen.ScreenshotOf;
import driverfactory.Driver;
import pages.SalesForce.GoogleHomePage;
import utilities.InitTests;
import verify.SoftAssertions;


public class TestGoogleSearchInFF extends InitTests {
	Driver driverObj=new Driver();
	WebDriver driver = null;
	WebDriver webdriver = null;
	  ExtentTest test=null;
/*	@BeforeClass
	  @Parameters({"nodeUrl"})
	public void init(String nodeUrl) throws Exception
	{
		System.out.println(nodeUrl);
		test = reports.createTest("testSearchInChrome " +nodeUrl);
		test.assignCategory("Grid");
		 WebDriver webdriver=driverObj.initWebDriver( BASEURL,"FF","local", "");
		 driver=driverObj.getEventDriver(webdriver,test);
	}*/
		@Test( enabled = true) 
		public void testSearchInFirefox() throws Exception {
		
			try {
				test = reports.createTest("Application2--testSearchInFirefox");
				test.assignCategory("smoke");
				webdriver=driverObj.initWebDriver("https://www.google.co.in","FF","local","");
				 driver=driverObj.getEventDriver(webdriver,test);
				GoogleHomePage ghomeChrome=new GoogleHomePage(driver);
				ghomeChrome.search("selenium");
				waitForElementToDisplay(ghomeChrome.getFirstLnk());
				verifyElementTextContains(ghomeChrome.getFirstLnk(), "selenium",test);

			} catch (Error e) {
				e.printStackTrace();
				SoftAssertions.fail(e, driverObj.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
				ATUReports.add("testSearch()", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
				softAssert.assertAll();

				

			} catch (Exception e) {
				e.printStackTrace();
				SoftAssertions.fail(e, driverObj.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
				ATUReports.add("testSearch()", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
				softAssert.assertAll();


			} 
			finally
			{
				reports.flush();
				driver.close();

			}
	}
	


}
