package smoke.application1.tests;


import org.openqa.selenium.WebDriver;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentTest;

import atu.testng.reports.ATUReports;
import atu.testng.reports.logging.LogAs;
import atu.testng.selenium.reports.CaptureScreen;
import atu.testng.selenium.reports.CaptureScreen.ScreenshotOf;
import driverfactory.Driver;
import pages.SalesForce.DashBoardPage;
import pages.SalesForce.GoogleHomePage;
import utilities.InitTests;
import verify.SoftAssertions;

import static driverfactory.Driver.waitForElementToDisplay;
import static utilities.MyExtentReports.reports;
import static verify.SoftAssertions.verifyElementTextContains;

public class TestGoogleSearchInIE extends InitTests {
	DashBoardPage home;
	Driver driverFact=new Driver();
	WebDriver driver = null;
	WebDriver webdriver = null;
@BeforeClass
public void beforeClass() throws Exception
{
	webdriver=driverFact.initWebDriver("https://www.google.co.in","IE","local","");

}
	@Test( enabled = true)
	public void testSearchInIE() throws Exception {

		ExtentTest test=null;
		try {
			 test = reports.createTest("Applicaiton1--testSearchInIE");
			test.assignCategory("smoke");
			driver=driverFact.getEventDriver(webdriver,test);
			GoogleHomePage ghomeIe=new GoogleHomePage(driver);
			ghomeIe.search("selenium");
			waitForElementToDisplay(ghomeIe.getFirstLnk());
			verifyElementTextContains(ghomeIe.getFirstLnk(), "selenium",test);
		} catch (Error e) {
			e.printStackTrace();
			SoftAssertions.fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()), test);
			ATUReports.add("testSearch()", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
			softAssert.assertAll();

			
 
		} catch (Exception e) {
			e.printStackTrace();
			SoftAssertions.fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()), test);
			ATUReports.add("testSearch()", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
			softAssert.assertAll();


		} 
		finally
		{
			reports.flush();
			driver.close();

		}
	}

	@Test( enabled = false)
	public void testSearchInIE1() throws Exception {
	
		ExtentTest test=null;
		try {
			 test = reports.createTest("testSearchInIE1");
			test.assignCategory("smoke");
			 driver=driverFact.getEventDriver(webdriver,test);
			GoogleHomePage ghomeIe=new GoogleHomePage(driver);
			ghomeIe.search("selenium");
			waitForElementToDisplay(ghomeIe.getFirstLnk());
			verifyElementTextContains(ghomeIe.getFirstLnk(), "selenium",test);
		} catch (Error e) {
			e.printStackTrace();
			SoftAssertions.fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()), test);
			ATUReports.add("testSearch()", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
			softAssert.assertAll();

			
 
		} catch (Exception e) {
			e.printStackTrace();
			SoftAssertions.fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()), test);
			ATUReports.add("testSearch()", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
			softAssert.assertAll();


		} 
		finally
		{
			reports.flush();
			driver.close();

		}
	}
}
